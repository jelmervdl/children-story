<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('auto_detect_line_endings', true);
ini_set('memory_limit', '512M');
set_time_limit(0);

date_default_timezone_set('Europe/Amsterdam');
setlocale(LC_ALL, 'nl_NL');

require_once 'config.php';
require_once 'lib/php/PHPExcel.php';
require_once 'lib/php/PHPExcel/Writer/Excel2007.php';

define('INDEX_OBJECT', 0);
define('INDEX_VERSION', 1);
define('INDEX_SENTENCE', 2);
define('INDEX_CONDITION', 3);
define('INDEX_PRONOUN', 4);
define('INDEX_REFERENT', 5);
define('INDEX_CORRECT', 6);

if (!class_exists('XMLWriter'))
    throw new Exception('Missing dependency: XMLWriter');

$animal_key = array(
    'monkey' => 'Aap',
    'elephant' => 'Olifant'
);

$setup = array(
    'a8a'               => array('handstand',           'a',    'Zij maakte een handstand.',                            '',             'Zij',  '',             'Aap'),
    'a10a'              => array('rondje',              'a',    'Hij liep een rondje om de huisjes.',                   '',             'Hij',  '',             'Olifant'),
    'a12a'              => array('touwtje',             'a',    'Hij ging touwtje springen.',                           '',             'Hij',  '',             'Olifant'),
    'a14a'              => array('koprol',              'a',    'Zij maakte een koprol.',                               '',             'Zij',  '',             'Aap'),
    'a8b'               => array('handstand',           'b',    'Hij maakte een handstand.',                            '',             'Hij',  '',             'Olifant'),
    'a10b'              => array('rondje',              'b',    'Zij liep een rondje om de huisjes.',                   '',             'Zij',  '',             'Aap'),
    'a12b'              => array('touwtje',             'b',    'Zij ging touwtje springen.',                           '',             'Zij',  '',             'Aap'),
    'a14b'              => array('koprol',              'b',    'Hij maakte een koprol.',                               '',             'Hij',  '',             'Olifant'),
    'voetbal_a'         => array('voetbal',             'a',    'Anita Aap zei Ik heb ook zo\'n voetbal.',              'Direct',       'Ik',   'Speaker',      'Aap'),
    'boek_a'            => array('boek',                'a',    'Anita Aap zei Ik heb ook zo\'n boek.',                 'Direct',       'Ik',   'Speaker',      'Aap'),
    'gitaar_a'          => array('gitaar',              'a',    'Anita Aap zei Ik heb ook zo\'n gitaar.',               'Direct',       'Ik',   'Speaker',      'Aap'),
    'zonnebril_a'       => array('zonnebril',           'a',    'Oscar Olifant zei Ik heb ook zo\'n zonnebril.',        'Direct',       'Ik',   'Speaker',      'Olifant'),
    'pen_a'             => array('pen',                 'a',    'Oscar Olifant zei Ik heb ook zo\'n pen.',              'Direct',       'Ik',   'Speaker',      'Olifant'),
    'sjaal_a'           => array('sjaal',               'a',    'Oscar Olifant zei Ik heb ook zo\'n sjaal.',            'Direct',       'Ik',   'Speaker',      'Olifant'),
    'paraplu_a'         => array('paraplu',             'a',    'Anita Aap zei Jij hebt ook zo\'n paraplu.',            'Direct',       'Jij',  'Addressee',    'Olifant'),
    'kopje_a'           => array('kopje',               'a',    'Anita Aap zei Jij hebt ook zo\'n kopje.',              'Direct',       'Jij',  'Addressee',    'Olifant'),
    'auto_a'            => array('auto',                'a',    'Anita Aap zei Jij hebt ook zo\'n auto .',              'Direct',       'Jij',  'Addressee',    'Olifant'),
    'vliegtuig_a'       => array('vliegtuig',           'a',    'Oscar Olifant zei Jij hebt ook zo\'n vliegtuig.',      'Direct',       'Jij',  'Addressee',    'Aap'),
    'lepel_a'           => array('lepel',               'a',    'Oscar Olifant zei Jij hebt ook zo\'n lepel.',          'Direct',       'Jij',  'Addressee',    'Aap'),
    'emmer_a'           => array('emmer',               'a',    'Oscar Olifant zei Jij hebt ook zo\'n emmer.',          'Direct',       'Jij',  'Addressee',    'Aap'),
    'vlag_a'            => array('vlag',                'a',    'Anita Aap zei dat zij ook zo\'n vlag heeft.',          'Indirect',     'Zij',  'Speaker',      'Aap'),
    'hoed_a'            => array('hoed',                'a',    'Anita Aap zei dat zij ook zo\'n hoed heeft.',          'Indirect',     'Zij',  'Speaker',      'Aap'),
    'tandenborstel_a'   => array('tandenborstel',       'a',    'Anita Aap zei dat zij ook zo\'n tandenborstel heeft.', 'Indirect',     'Zij',  'Speaker',      'Aap'),
    'schaar_a'          => array('schaar',              'a',    'Oscar Olifant zei dat hij ook zo\'n schaar heeft.',    'Indirect',     'Hij',  'Speaker',      'Olifant'),
    'sleutel_a'         => array('sleutel',             'a',    'Oscar Olifant zei dat hij ook zo\'n sleutel heeft.',   'Indirect',     'Hij',  'Speaker',      'Olifant'),
    'stoel_a'           => array('stoel',               'a',    'Oscar Olifant zei dat hij ook zo\'n stoel heeft.',     'Indirect',     'Hij',  'Speaker',      'Olifant'),
    'rugzak_a'          => array('rugzak',              'a',    'Oscar Olifant zei dat zij ook zo\'n rugzak heeft.',    'Indirect',     'Zij',  'Addressee',    'Aap'),
    'lamp_a'            => array('lamp',                'a',    'Oscar Olifant zei dat zij ook zo\'n lamp heeft.',      'Indirect',     'Zij',  'Addressee',    'Aap'),
    'schoen_a'          => array('schoen',              'a',    'Oscar Olifant zei dat zij ook zo\'n schoen heeft.',    'Indirect',     'Zij',  'Addressee',    'Aap'),
    'bezem_a'           => array('bezem',               'a',    'Anita Aap zei dat hij ook zo\'n bezem heeft.',         'Indirect',     'Hij',  'Addressee',    'Olifant'),
    'klok_a'            => array('klok',                'a',    'Anita Aap zei dat hij ook zo\'n klok heeft.',          'Indirect',     'Hij',  'Addressee',    'Olifant'),
    'skateboard_a'      => array('skateboard',          'a',    'Anita Aap zei dat hij ook zo\'n skateboard heeft.',    'Indirect',     'Hij',  'Addressee',    'Olifant'),
    'voetbal_b'         => array('voetbal',             'b',    'Oscar Olifant zei dat hij ook zo\'n voetbal heeft.',   'Indirect',     'Hij',  'Speaker',      'Olifant'),
    'boek_b'            => array('boek',                'b',    'Oscar Olifant zei dat hij ook zo\'n boek heeft.',      'Indirect',     'Hij',  'Speaker',      'Olifant'),
    'gitaar_b'          => array('gitaar',              'b',    'Oscar Olifant zei dat hij ook zo\'n gitaar heeft.',    'Indirect',     'Hij',  'Speaker',      'Olifant'),
    'zonnebril_b'       => array('zonnebril',           'b',    'Anita Aap zei dat zij ook zo\'n zonnebril heeft.',     'Indirect',     'Zij',  'Speaker',      'Aap'),
    'pen_b'             => array('pen',                 'b',    'Anita Aap zei dat zij ook zo\'n pen heeft.',           'Indirect',     'Zij',  'Speaker',      'Aap'),
    'sjaal_b'           => array('sjaal',               'b',    'Anita Aap zei dat zij ook zo\'n sjaal heeft.',         'Indirect',     'Zij',  'Speaker',      'Aap'),
    'paraplu_b'         => array('paraplu',             'b',    'Oscar Olifant zei dat zij ook zo\'n paraplu heeft.',   'Indirect',     'Zij',  'Addressee',    'Aap'),
    'kopje_b'           => array('kopje',               'b',    'Oscar Olifant zei dat zij ook zo\'n kopje heeft.',     'Indirect',     'Zij',  'Addressee',    'Aap'),
    'auto_b'            => array('auto',                'b',    'Oscar Olifant zei dat zij ook zo\'n auto heeft.',      'Indirect',     'Zij',  'Addressee',    'Aap'),
    'vliegtuig_b'       => array('vliegtuig',           'b',    'Anita Aap zei dat hij ook zo\'n vliegtuig heeft.',     'Indirect',     'Hij',  'Addressee',    'Olifant'),
    'lepel_b'           => array('lepel',               'b',    'Anita Aap zei dat hij ook zo\'n lepel heeft.',         'Indirect',     'Hij',  'Addressee',    'Olifant'),
    'emmer_b'           => array('emmer',               'b',    'Anita Aap zei dat hij ook zo\'n emmer heeft.',         'Indirect',     'Hij',  'Addressee',    'Olifant'),
    'vlag_b'            => array('vlag',                'b',    'Oscar Olifant zei Ik heb ook zo\'n vlag.',             'Direct',       'Ik',   'Speaker',      'Olifant'),
    'hoed_b'            => array('hoed',                'b',    'Oscar Olifant zei Ik heb ook zo\'n hoed.',             'Direct',       'Ik',   'Speaker',      'Olifant'),
    'tandenborstel_b'   => array('tandenborstel',       'b',    'Oscar Olifant zei Ik heb ook zo\'n tandenborstel.',    'Direct',       'Ik',   'Speaker',      'Olifant'),
    'schaar_b'          => array('schaar',              'b',    'Anita Aap zei Ik heb ook zo\'n schaar.',               'Direct',       'Ik',   'Speaker',      'Aap'),
    'sleutel_b'         => array('sleutel',             'b',    'Anita Aap zei Ik heb ook zo\'n sleutel.',              'Direct',       'Ik',   'Speaker',      'Aap'),
    'stoel_b'           => array('stoel',               'b',    'Anita Aap zei Ik heb ook zo\'n stoel.',                'Direct',       'Ik',   'Speaker',      'Aap'),
    'rugzak_b'          => array('rugzak',              'b',    'Anita Aap zei Jij hebt ook zo\'n rugzak.',             'Direct',       'Jij',  'Addressee',    'Olifant'),
    'lamp_b'            => array('lamp',                'b',    'Anita Aap zei Jij hebt ook zo\'n lamp.',               'Direct',       'Jij',  'Addressee',    'Olifant'),
    'schoen_b'          => array('schoen',              'b',    'Anita Aap zei Jij hebt ook zo\'n schoen.',             'Direct',       'Jij',  'Addressee',    'Olifant'),
    'bezem_b'           => array('bezem',               'b',    'Oscar Olifant zei Jij hebt ook zo\'n bezem.',          'Direct',       'Jij',  'Addressee',    'Aap'),
    'klok_b'            => array('klok',                'b',    'Oscar Olifant zei Jij hebt ook zo\'n klok.',           'Direct',       'Jij',  'Addressee',    'Aap'),
    'skateboard_b'      => array('skateboard',          'b',    'Oscar Olifant zei Jij hebt ook zo\'n skateboard.',     'Direct',       'Jij',  'Addressee',    'Aap')
);

interface Decorator
{
    public function decorate(array $row);
}

class DummyDecorator implements Decorator
{
    public function decorate(array $row)
    {
        return $row;
    }
}

class ConfigurationDecorator implements Decorator
{
    public function __construct(array $config)
    {
        $this->configurations = array();

        // Copy all configurations to a flat version
        foreach ($config as $section => $configurations)
        {
            foreach ($configurations as $configuration)
            {
                $configuration['section'] = $section;
                $configuration['sentence'] = trim(@iconv('UTF-8', 'ASCII//TRANSLIT', $configuration['sentence']));
                $this->configurations[$configuration['code']] = $configuration;
            }
        }
    }

    public function decorate(array $row)
    {
        $configuration = $this->configurations[$row['act_id']];

        $configuration['correct'] = $row['choice'] == $configuration['correct_position'] ? '1' : '0';

        return array_merge($row, $configuration);
    }
}

function export_to_r(PDO $db)
{
    $query = $db->query("
        SELECT
            CONCAT('p', p.subject_id) as subject_id,
            p.age,
            p.sex,
            p.browser,
            p.platform,
            p.branch as version,
            p.submitted,
            n.language,
            m.id as measurement_id,
            m.act_id,
            m.choice,
            m.start_time,
            m.stop_time,
            (m.stop_time - m.start_time) as difference
        FROM
            personal_details p
        LEFT JOIN
            native_tongue n ON
            n.subject_id = p.subject_id
        RIGHT JOIN
            measurements m ON
            m.subject_id = p.subject_id
        WHERE
            p.submitted IS NOT NULL");

    return $query;
}

function export_to_excel(PDO $db)
{
    global $setup, $animal_key;

    $query = $db->query("
        SELECT
            CONCAT('p', p.subject_id) as ID,
            GROUP_CONCAT(n.language) as Language,
            p.age as Age,
            p.sex as Gender,
            p.branch as Version,
            p.submitted as 'Test time',
            m.act_id,
            m.choice,
            (m.stop_time - m.start_time) as response_time
        FROM
            personal_details p
        RIGHT JOIN
            measurements m
            ON m.subject_id = p.subject_id
        LEFT JOIN
            native_tongue n ON
            n.subject_id = p.subject_id
        WHERE
            p.submitted IS NOT NULL
            AND p.branch = 'ipad-story'
        GROUP BY
            m.id
        ORDER BY
            m.start_time ASC,
            p.submitted ASC");

    // Creating a workbook
    $workbook = new PHPExcel();
    $workbook->setActiveSheetIndex(0);

    $now = date('YmdHis');

    $worksheet = $workbook->getActiveSheet();
    $worksheet->setTitle('Exported data');
    
    $row = 1;
    $col = 0;

    $version = '?';

    $trialcount = array();
    $experience = array();
    $position = array();
    $answer_ownership = array();

    // Header row
    foreach (array('ID', 'Language', 'Age', 'Gender', 'Version', 'Test time', 'Item', 'Object', 'Version', 'Condition', 'Pronoun', 'Referent', 'RT', 'Correct', 'Trial', 'Position', 'Experience', 'Distance', 'Memory adequate') as $column)
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $column, PHPExcel_Cell_DataType::TYPE_STRING);

    for ($i = 0;;++$i)
    {
        $row = $i + 2;
        $col = 0;

        $data = $query->fetch(PDO::FETCH_ASSOC);

        $monkey_position = '';

        if (!$data)
            break;

        if (preg_match('/^(a(\d{1,2})(a|b))(?:_m(left|right))?$/', $data['act_id'], $match)) {
            $context = $setup[$match[1]];
            $version = $match[3];
            $item = $context[INDEX_OBJECT] . '_' . $version;
            $question = 'a';

            if (!empty($match[4]))
                $monkey_position = 'm' . $match[4]{0}; // 'm' for monkey plus 'l' for left or 'r' for right
        }
        elseif (preg_match('/^(q|m)_([a-z]+)(?:_m(left|right))?$/', $data['act_id'], $match)) {
            $context = $setup[$match[2] . '_'. $version];
            $question = $match[1];
            
            $item = $match[1] == 'q'
                ? $match[2] . '_' . $version
                : $match[1] . '_' . $match[2];

            if (!empty($match[3]))
                $monkey_position = 'm' . $match[3]{0};
        }
        else
            throw new Exception('Cannot extract information from act_id ' . $data['act_id']);

        $is_correct = $animal_key[$data['choice']] == $context[INDEX_CORRECT];
            
        foreach (array('ID', 'Language', 'Age', 'Gender', 'Version', 'Test time') as $column)
            $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $data[$column], PHPExcel_Cell_DataType::TYPE_STRING);

        // Item
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $item, PHPExcel_Cell_DataType::TYPE_STRING);

        // Object
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $context[INDEX_OBJECT], PHPExcel_Cell_DataType::TYPE_STRING);

        // Version
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $context[INDEX_VERSION], PHPExcel_Cell_DataType::TYPE_STRING);

        // Condition
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $context[INDEX_CONDITION], PHPExcel_Cell_DataType::TYPE_STRING);
        
        // Pronoun
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $context[INDEX_PRONOUN], PHPExcel_Cell_DataType::TYPE_STRING);

        // Referent
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $context[INDEX_REFERENT], PHPExcel_Cell_DataType::TYPE_STRING);

        // Reaction time
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $data['response_time'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

        // Correct
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $is_correct ? 1 : 0, PHPExcel_Cell_DataType::TYPE_NUMERIC);

        // Trial
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $trialcount[$data['ID']] = isset($trialcount[$data['ID']]) ? $trialcount[$data['ID']] + 1 : 1, PHPExcel_Cell_DataType::TYPE_NUMERIC);

        // Position
        $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $monkey_position, PHPExcel_Cell_DataType::TYPE_STRING);

        // Experience: How often a participant has seen a certain type of test item (Direct-speaker, Direct-addressee, Indirect-speaker, indirect-addressee) before, every participant sees a certain type of test item 6 times (only applicable for main test, not memory test)
        if ($question == 'q') {
            $xp_key = sprintf('%s_%s_%s', $data['ID'], $context[INDEX_CONDITION], $context[INDEX_REFERENT]);
            $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $experience[$xp_key] = isset($experience[$xp_key]) ? $experience[$xp_key] + 1 : 1, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        }
        else
            $col++;

        // Distance: Distance between test item with a certain object and item in memory test with that object
        if ($question == 'q') {
            $position[$context[INDEX_OBJECT]] = $trialcount[$data['ID']];
            $col++;
        }
        else if ($question == 'm') {
            $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $trialcount[$data['ID']] - $position[$context[INDEX_OBJECT]], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        }
        else
            $col++;

        // Memory adequate: Compare participants answers in memory test with their answers in main test: 1 if their answer in memory test matches their previous answer, 0 if it does not
        if ($question == 'q') {
            $answer_ownership[$context[INDEX_OBJECT]] = $data['choice'];
            $col++;
        }
        else if ($question == 'm') {
            $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $answer_ownership[$context[INDEX_OBJECT]] == $data['choice'] ? 1 : 0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        }

    }

    $writer = new PHPExcel_Writer_Excel2007($workbook);

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
    header("Content-type: application/vnd.ms-excel;charset=UTF-8"); 
    header("Content-Disposition: attachment; filename=\"{$now}.xls\"");
    header("Cache-control: private");

    $writer->save('php://output');
}

function evaluation_of_mistakes_dir_ind($choice, $correct, $pronoun)
{
    switch ($pronoun)
    {
        case 'hij':
            return $choice == 'option-1' && $correct == 'option-3'
                || $choice == 'option-3' && $correct == 'option-1';

        case 'ik':
            return $choice == 'option-2' && $correct == 'option-1'
                || $choice == 'option-2' && $correct == 'option-3';

        case 'jij':
            return $choice == 'option-1' && $correct == 'option-2'
                || $choice == 'option-3' && $correct == 'option-2';
    }
}

function print_results_as_csv(PDOStatement $stmt, Decorator $decorator = null)
{
    $now = date('YmdHis');

    header("Content-Type: application/octet-stream");
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"{$now}.csv\"");
    
    ob_start("ob_gzhandler");
    
    $printed_headers = false;

    if (!$decorator)
        $decorator = new DummyDecorator();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        $row = $decorator->decorate($row);

        // If this is the first row, print the column headers
        if (!$printed_headers) {
            echo implode(",", array_keys($row)) . "\n";
            $printed_headers = true;
        }

        // Enclose text fields in quotes, and escape quotes in these fields.
        foreach (array('browser', 'platform', 'language', 'submitted') as $field)
            $row[$field] = sprintf('"%s"', str_replace('"', '""', $row[$field]));

        // print the row.
        echo implode(",", $row) . "\n";
    }
}

function print_results_as_excel(PDOStatement $stmt, Decorator $decorator = null)
{
    $now = date('YmdHis');

    $printed_headers = false;

    if (!$decorator)
        $decorator = new DummyDecorator();

    // Creating a workbook
    $workbook = new PHPExcel();
    $workbook->setActiveSheetIndex(0);

    $now = date('YmdHis');

    $worksheet = $workbook->getActiveSheet();
    $worksheet->setTitle('Exported data');
    
    for ($row = 1, $col = 0; $data = $stmt->fetch(PDO::FETCH_ASSOC); ++$row, $col = 0)
    {
        $data = $decorator->decorate($data);
        
        // If this is the first row, print the column headers
        if (!$printed_headers) {
            foreach (array_keys($data) as $column)
                $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $column, PHPExcel_Cell_DataType::TYPE_STRING);
            $printed_headers = true;
            $row++;
            $col = 0;
        }

        // Enclose text fields in quotes, and escape quotes in these fields.
        $numeric_colums = array('start_time', 'stop_time', 'difference');
        foreach ($data as $column => $field)
            $worksheet->setCellValueExplicitByColumnAndRow($col++, $row, $field,
                in_array($column, $numeric_colums) ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING);
    }

    $writer = new PHPExcel_Writer_Excel2007($workbook);
    
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
    header("Content-type: application/vnd.ms-excel;charset=UTF-8"); 
    header("Content-Disposition: attachment; filename=\"{$now}.xls\"");
    header("Cache-control: private");

    $writer->save('php://output');
}


function fetch_one(PDOStatement $stmt)
{
    $values = $stmt->fetch(PDO::FETCH_NUM);

    return $values ? $values[0] : null;
}

function print_index(PDO $db)
{
    $last_submission = fetch_one($db->query("SELECT MAX(submitted) as last_submission FROM personal_details WHERE branch = 'ipad-story'"));

    $number_of_submissions = fetch_one($db->query("SELECT COUNT(subject_id) FROM personal_details WHERE branch = 'ipad-story'"));
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Export</title>
        <style>
            body {
                font: 14px sans-serif;
            }
        </style>
    </head>
    <body>
        <p>Submissions: <strong><?php echo $number_of_submissions?></strong></p>
        <p>Last submission: <strong><?php echo $last_submission?></strong></p>
        <p>Export to:</p>
        <ul>
            <li><a href="export.php?target=r-csv">Raw data CSV</a></li>
            <li><a href="export.php?target=r-excel">Raw data Excel</a> (may be slow)</li>
            <li><a href="export.php?target=excel">Excel sheet</a></li>
        </ul>
    </body>
</html>
<?php
}

function main()
{
    $db = config_pdo();

    switch (@$_GET['target'])
    {
        case 'r-csv':
            print_results_as_csv(export_to_r($db));
            break;

        case 'r-excel':
            print_results_as_excel(export_to_r($db));
            break;

        case 'excel':
            export_to_excel($db);
            break;
        
        default:
            print_index($db);
            break;
    }
}

main();
