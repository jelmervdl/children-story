(function() {

// Disable scrolling altogether
document.ontouchmove = function(event){
	event.preventDefault();
};

var getCharacter = function getCharacter(option) {
	var characters = ['monkey', 'elephant'];

	for (var i = 0; i < characters.length; ++i)
		if (option.classList.contains(characters[i]))
			return characters[i];

	return null;
};

var getPosition = function getPosition(character) {
	var match;

	for (var i = 0; i < character.classList.length; ++i)
		if (match = /^(?:(?!facing)[a-z]+-)?(left|right)$/.exec(character.classList[i]))
			return match[1];

	return null;
};

var params = {
	pauseAfterTestSentence: 1250,
	pauseBeforeStory: 500,
	pauseAfterStory: 500
};

var version = 'a';

var characters = [
	'monkey',
	'elephant'
];

var objects = [
	'auto',
	'bezem',
	'boek',
	'emmer',
	'gitaar',
	'hoed',
	'klok',
	'kopje',
	'lamp',
	'lepel',
	'paraplu',
	'pen',
	'rugzak',
	'schaar',
	'schoen',
	'sjaal',
	'skateboard',
	'sleutel',
	'stoel',
	'tandenborstel',
	'vlag',
	'vliegtuig',
	'voetbal',
	'zonnebril'
].shuffle();

var templates = {
	mark: function(name) {
		return function(stage, next) {
			this.mark(name);
			next();
		};
	},
	pause: function(pause) {
		return function(stage, next) {
			setTimeout(next, pause);
		};
	},
	story: function(asset) {
		return function(stage, next) {
			this.mark('story');
			audio.play(asset, next);
		};
	},
	question: function(question) {
		return function(stage, next) {
			this.mark('question');
			audio.play(question, next);
		};
	},
	task: function(question) {
		return function(stage, next) {
			var act = this;

			var positionOfMonkey = getPosition(stage.querySelector('.character.monkey'));

			var measurement = new n.Measurement(question + '_m' + positionOfMonkey);
			
			this.clickOnce(stage.querySelectorAll('.character.elephant, .character.monkey'), function(option) {
				measurement.stop(getCharacter(option));
				next();
			});

			this.setTimeout(function() {
				act.clickOnce([$('replay-button')], function() {
					act.jump('story');
				});
			}, 10000);

			measurement.start();
		};
	},
	setObject: function(object) {
		return function(stage, next) {
			var element = stage.querySelector('.object');

			if (!element)
				throw Error('templates.setObject: Cannot find object in stage ' + stage.id);

			objects.forEach(bind(element.classList, 'remove'));

			element.classList.add(object);
			next();
		};
	},
	swapCharacters: function () {
		return function (stage, next) {
			if (Math.random() < 0.5)
				return next();

			var monkey = stage.querySelector('.character.monkey');
			var elephant = stage.querySelector('.character.elephant');

			if (monkey && elephant) {
				monkey.classList.remove('monkey');
				elephant.classList.remove('elephant');
				monkey.classList.add('elephant');
				elephant.classList.add('monkey');
			}

			next();
		}
	},
	test: function(story, object) {
		return [
			templates.setObject(object),
			templates.swapCharacters(),
			templates.pause(params.pauseBeforeStory),
			templates.story(story),
			templates.pause(params.pauseAfterStory),
			templates.story(object + '_' + version),
			templates.pause(params.pauseAfterTestSentence),
			templates.question('q_' + object),
			templates.task('q_' + object)
		];
	},
	single: function(story) {
		return [
			templates.pause(params.pauseBeforeStory),
			templates.story(story),
			templates.pause(params.pauseAfterStory)
		];
	}
};

var acts = [];

var audio = new n.AudioSprite();

// Login act
acts.push(function(next) {
	new n.Act({
		stage: document.getElementById('login-stage'),
		scenes: [
			function(stage, next) {
				clickOnce(stage.querySelectorAll('.start-button'), function(button) {
					version = button.getAttribute('data-version').toLowerCase();
					next();
				});
			}
		]
	}).play(next);
});

// Character introduction
acts.push(function(next) {

	var a2 = [
		templates.pause(1000),
		// Wie is aap?
		function(stage, next) {
			this.mark('a2');
			audio.play('a2', next);
		},
		function(stage, next) {
			var act = this;
			clickOnce(stage.querySelectorAll('.character'), function(option) {
				if (option.classList.contains('monkey'))
					next();
				else
					audio.play('a6', function() {
						setTimeout(function() {
							act.jump('a2');
						}, 1000);
					});
			});
		}
	];

	var a3 = [
		templates.pause(1000),
		// Wie olifant
		function(stage, next) {
			this.mark('a3');
			audio.play('a3', next);
		},
		function(stage, next) {
			var act = this;
			
			clickOnce(stage.querySelectorAll('.character'), function(option) {
				if (option.classList.contains('elephant'))
					next();
				else
					audio.play('a6', function() {
						setTimeout(function() {
							act.jump('a3');
						}, 1000);
					});
			});
		}
	];

	var a5 = [
		templates.pause(1000),
		// Wie is een jongen?
		function(stage, next) {
			this.mark('a5');
			audio.play('a5', next);
		},
		function(stage, next) {
			var act = this;
			
			clickOnce(stage.querySelectorAll('.character'), function(option) {
				if (option.classList.contains('elephant'))
					next();
				else
					audio.play('a6', function() {
						setTimeout(function() {
							act.jump('a5');
						}, 1000);
					});
			});
		}
	];

	var a4 = [
		templates.pause(1000),
		// Wie is een meisje?
		function(stage, next) {
			this.mark('a4');
			audio.play('a4', next);
		},
		function(stage, next) {
			var act = this;
			
			clickOnce(stage.querySelectorAll('.character'), function(option) {
				if (option.classList.contains('monkey'))
					next();
				else
					audio.play('a6', function() {
						setTimeout(function() {
							act.jump('a4');
						}, 1000);
					});
			});
		}
	];

	var scenes = [// Kijk! Beestjes
		templates.pause(1500), // tijd om de tablet aan het kind te geven
		templates.story('a1')
	];

	switch (version) {
		case 'a':
			scenes = scenes.concat(a2, a3, [templates.swapCharacters()], a4, a5);
			break;

		case 'b':
			scenes = scenes.concat(a3, a2, [templates.swapCharacters()], a5, a4);
			break;
	}

	new n.Act({
		stage: document.getElementById('stage1a'),
		scenes: scenes
	}).play(next);
});

// Oscar Olifant en Anita Aap zijn beste vriendjes. Ze wonen naast
// elkaar in twee huisjes. Oscar Olifant woont in een huisje van
// steen en Anita Aap woont in een huisje van hout. Ze zien elkaar
// elke dag en samen beleven ze leuke avonturen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage2'),
		scenes: templates.single('a7_1')
	}).play(next);
});

// Op een dag werden Anita Aap en Oscar Olifant vroeg wakker. Zoals
// altijd begonnen ze de dag met ochtendgymnastiek. Oscar en Anita
// deden allebei iets.
acts.push(function(next) {
	new n.Act({
		stage: $('stage1b'),
		scenes: [
			templates.pause(1500),

			// Op een dag ...
			templates.story('a7_2'),
			templates.pause(1000),
			
			// Zij maakt een handstand
			templates.swapCharacters(),
			templates.story('a8' + version),
			templates.pause(params.pauseAfterTestSentence),
			// Wie maakt een handstand?
			templates.question('a9'),
			templates.task('a8' + version),
			
			// Hij liep een rondje om het huis
			templates.swapCharacters(),
			templates.pause(1000),
			templates.story('a10' + version),
			// Wie liep een rondje om het huis?
			templates.pause(params.pauseAfterTestSentence),
			templates.question('a11'),
			templates.task('a10' + version),
			
			// Hij ging touwtje springen.
			templates.swapCharacters(),
			templates.pause(1000),
			templates.story('a12' + version),
			templates.pause(params.pauseAfterTestSentence),
			templates.question('a13'),
			templates.task('a12' + version),

			// Zij maakte een koprol
			templates.swapCharacters(),
			templates.pause(1000),
			templates.story('a14' + version),
			templates.pause(params.pauseAfterTestSentence),
			templates.question('a15'),
			templates.task('a14' + version)
		]
	}).play(next);
});

// Na het sporten, besloten Oscar Olifant en Anita Aap samen een
// wandeling te gaan maken. Het was een mooie dag en het was
// ‘s ochtends al lekker warm. Ze liepen op blote voeten door het
// gras.
acts.push(function(next) {
	new n.Act({
		stage: $('stage3'),
		scenes: templates.single('s1_1')
	}).play(next);
});

// Daar zagen ze iets in het gras liggen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage4'),
		scenes: templates.test('s1_2', objects[0])
	}).play(next);
});

// Er lag nog iets in het gras.
acts.push(function(next) {
	new n.Act({
		stage: $('stage5a'),
		scenes: templates.test('s2', objects[1])
	}).play(next);
});

// Anita en Oscar liepen en paar passen verder en kwamen nog iets tegen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage4'),
		scenes: templates.test('s3', objects[2])
	}).play(next);
});

// Oscar en Anita liepen verder. Ze kwamen uit op een pad en volgden het een stukje.
acts.push(function(next) {
	new n.Act({
		stage: $('stage6'),
		scenes: templates.single('s4_1')
	}).play(next);
});

// Opeens zagen ze iets op het pad liggen. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage7a'),
		scenes: templates.test('s4_2', objects[3])
	}).play(next);
});

// En er lag ook nog iets naast het pad.
acts.push(function(next) {
	new n.Act({
		stage: $('stage7b'),
		scenes: templates.test('s5', objects[4])
	}).play(next);
});

// Anita en Oscar liepen nog een stukje verder.
acts.push(function(next) {
	new n.Act({
		stage: $('stage7c'),
		scenes: templates.single('s6_1')
	}).play(next);
});

// Bij het kruispunt lag nog iets op de grond.
acts.push(function(next) {
	new n.Act({
		stage: $('stage8'),
		scenes: templates.test('s6_2', objects[5])
	}).play(next);
});

// Ze gingen verder. Anita Aap floot een liedje en Oscar Olifant
// toeterde mee met zijn slurf. Ze hadden het heel gezellig met
// elkaar. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage9'),
		scenes: templates.single('s7_1')
	}).play(next);
});

// Ineens zagen ze iets onder een boom liggen. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage10a'),
		scenes: templates.test('s7_2', objects[6])
	}).play(next);
});

// Oscar en Anita keken omhoog en zagen ook iets in de boom hangen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage10b'),
		scenes: templates.test('s8', objects[7])
	}).play(next);
});

// Ze liepen om de boom heen en zagen aan de andere kant ook nog
// iets in de boom hangen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage11'),
		scenes: templates.test('s9', objects[8])
	}).play(next);
});

// Toen begon het te regenen. Gelukkig was er een grot dichtbij.
// Anita Aap en Oscar Olifant liepen snel de donkere grot binnen
// en schuilden voor de regen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage12'),
		scenes: templates.single('s10_1')
	}).play(next);
});

// Toen hun ogen gewend waren aan het donker, zagen ze iets in
// de grot liggen. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage13'),
		scenes: templates.test('s10_2', objects[9])
	}).play(next);
});

// Oscar en Anita liepen verder de grot in en ontdekten nog iets
// op de grond.
acts.push(function(next) {
	new n.Act({
		stage: $('stage14'),
		scenes: templates.test('s11', objects[10])
	}).play(next);
});

// Achter een grote steen lag ook nog iets verstopt.
acts.push(function(next) {
	new n.Act({
		stage: $('stage15'),
		scenes: templates.test('s12', objects[11])
	}).play(next);
});

// Toen stopte het met regenen. Anita Aap en Oscar Olifant liepen
// de grot uit en gingen weer verder.
acts.push(function(next) {
	new n.Act({
		stage: $('stage16'),
		scenes: templates.single('s13_1')
	}).play(next);
});

// Ze kwamen aan bij een weiland.
// Tussen het gras groeiden bloemetjes in alle kleuren van de
// regenboog.
acts.push(function(next) {
	new n.Act({
		stage: $('stage17a'),
		scenes: templates.single('s13_2')
	}).play(next);
});

// Opeens zagen ze iets tussen de bloemetjes liggen. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage17b'),
		scenes: templates.test('s13_3', objects[12])
	}).play(next);
});

// Oscar en Anita liepen verder door het weiland. Ze stopten zo
// nu en dan om aan de bloemetjes te ruiken.
acts.push(function(next) {
	new n.Act({
		stage: $('stage18a'),
		scenes: templates.single('s14_1')
	}).play(next);
});

// Toen zagen ze nog iets tussen de bloemetjes liggen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage18b'),
		scenes: templates.test('s14_2', objects[13])
	}).play(next);
});

// Verderop achter een grote zonebloem lag ook nog iets.
acts.push(function(next) {
	new n.Act({
		stage: $('stage19'),
		scenes: templates.test('s15', objects[14])
	}).play(next);
});

// Anita Aap en Oscar Olifant liepen weer verder.
acts.push(function(next) {
	new n.Act({
		stage: $('stage3'),
		scenes: templates.single('s16_1')
	}).play(next);
});

// Na een tijdje kwamen ze bij een vijver.
acts.push(function(next) {
	new n.Act({
		stage: $('stage20a'),
		scenes: templates.single('s16_2')
	}).play(next);
});

// Ze zagen iets in de vijver liggen. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage20b'),
		scenes: templates.test('s16_3', objects[15])
	}).play(next);
});

// Ze vonden ook nog iets tussen het riet. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage20c'),
		scenes: templates.test('s17', objects[16])
	}).play(next);
});

// Het water van de vijver was lekker warm. Oscar en Anita sprongen
// in het water en spetterden elkaar nat.
acts.push(function(next) {
	new n.Act({
		stage: $('stage21a'),
		scenes: templates.single('s18_1')
	}).play(next);
});

// Ineens zagen ze iets in het water drijven. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage21b'),
		scenes: templates.test('s18_2', objects[17])
	}).play(next);
});

// Anita Aap en Oscar Olifant gingen weer uit het water en lieten
// zich opdrogen in de zon. Daarna liepen ze weer verder. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage3'),
		scenes: templates.single('s19_1')
	}).play(next);
});

// Opeens zagen ze iets voor een rots liggen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage22'),
		scenes: templates.test('s19_2', objects[18])
	}).play(next);
});

// Ze klommen op de rots en zagen op de top nog iets liggen.
acts.push(function(next) {
	new n.Act({
		stage: $('stage23a'),
		scenes: templates.test('s20', objects[19])
	}).play(next);
});

// Bovenop de rots waaide een frisse wind. Ze hadden een prachtig
// uitzicht.
acts.push(function(next) {
	new n.Act({
		stage: $('stage23b'),
		scenes: templates.single('s21_1')
	}).play(next);
});

// Oscar en Anita gingen aan de andere kant van de rots naa
// beneden.
acts.push(function(next) {
	new n.Act({
		stage: $('stage24a'),
		scenes: templates.single('s21_2')
	}).play(next);
});

// Daar lag ook iets.
acts.push(function(next) {
	new n.Act({
		stage: $('stage24b'),
		scenes: templates.test('s21_3', objects[20])
	}).play(next);
});

// Anita en Oscar liepen weer verder. Ze kwamen bij een beekje en
// liepen naar de brug.
acts.push(function(next) {
	new n.Act({
		stage: $('stage25'),
		scenes: templates.single('s22_1')
	}).play(next);
});

// Er lag iets op de brug.
acts.push(function(next) {
	new n.Act({
		stage: $('stage26'),
		scenes:  templates.test('s22_2', objects[21])
	}).play(next);
});

// Aan de andere kant van de brug lag ook iets.
acts.push(function(next) {
	new n.Act({
		stage: $('stage27'),
		scenes: templates.test('s23', objects[22])
	}).play(next);
});

// Ineens hoorden Oscar en Anita een vreemd geluid. Wat was dat?
// Ze liepen verder in de richting waar het geluid vandaan kwam.
acts.push(function(next) {
	new n.Act({
		stage: $('stage3'),
		scenes: templates.single('s24_1')
	}).play(next);
});

// Daar zat een hond tegen een boom te slapen. De hond snurkte
// heel hard. Vlak naast de hond lag weer iets in het gras. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage28'),
		scenes: templates.test('s24_2', objects[23])
	}).play(next);
});

// Dat was wel heel toevallig. Overal kwamen ze spulletjes tegen
// die er precies zo uitzagen als hun eigen spulletjes. Opeens
// hadden Anita en Oscar het door. Het waren hun eigen spulletjes!
// Maar hoe kon dat? Had de hond hun spulletjes gestolen?
acts.push(function(next) {
	new n.Act({
		stage: $('stage28'),
		scenes: templates.single('s25_1')
	}).play(next);
});

// Ze maakten de hond wakker en vroegen hem: “Heb jij onze
// spulletjes gestolen?”. Hond werd vuurrood en zei: “Ja, ik
// heb jullie spulletjes meegenomen. Het spijt me heel erg. Ik
// wilde alleen maar spelen.” Anita en Oscar dachten even na.
// “Wij zijn niet boos, hoor”, zei Anita. “Maar dan moet je wel
// al onze spulletjes weer terug brengen”, zei Oscar. Hond zei:
// “Ik zal alles meteen weer terugbrengen.”
acts.push(function(next) {
	new n.Act({
		stage: $('stage29'),
		scenes: templates.single('s25_2')
	}).play(next);
});

// Hond ging gelijk op pad. Hij zocht alle spulletjes bij elkaar
// en bracht ze naar de tuin van Anita en Oscar. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage30'),
		scenes: templates.single('s25_3')
	}).play(next);
});

// Maar nu heeft Hond een probleem! Welke dingen zijn van Oscar
// Olifant en welke van Anita Aap? Alles ligt door elkaar. Kun
// jij Hond helpen? Het geeft niet als je niet alles meer weet hoor. 
acts.push(function(next) {
	new n.Act({
		stage: $('stage31'),
		scenes: templates.single('s25_4')
	}).play(next);
});

// Memory task picture
acts.push(function(next) {
	var scenes = [templates.pause(1500)];

	objects.shuffle().forEach(function(object) {
		scenes.push(
			templates.swapCharacters(),
			templates.setObject(object),
			templates.pause(1000),
			templates.story('m_' + object),
			templates.task('m_' + object));
	});

	new n.Act({
		stage: $('stage32'),
		scenes: scenes
	}).play(next);
});


// Thank you! act, show a form
acts.push(function(next) {
	new n.Act({
		stage: document.getElementById('end-stage'),
		scenes: [
			templates.story('a16'),

			function(stage, next) {
				// that's it :)
				this.clickOnce(stage.querySelectorAll('button'), function() {
					var age, sex, native_tongue = [];

					age = stage.querySelector('input[name=age]').value;
					
					Array.prototype.forEach.call(stage.querySelectorAll('input[name=sex]'), function(option) {
						if (option.checked)
							sex = option.value;
					});

					Array.prototype.forEach.call(stage.querySelectorAll('input[name=native_tongue]'), function(option) {
						if (option.checked)
							native_tongue.push(option.value);
					});

					if (stage.querySelector('input[name=native_tongue_other]').value.trim())
						native_tongue.push(stage.querySelector('input[name=native_tongue_other]').value.trim());

					if (!age || !sex || native_tongue.length === 0)
						return false;
					
					n.Measurements.submit({
						'age': age,
						'sex': sex,
						'native_tongue': native_tongue,
						'browser': 'native android app 1',
						'platform': window.navigator.platform
					});
					
					next();
				});
			},
			function(stage, next) {
				stage.classList.add('done');
			}
		]
	}).play(next);
});

var main = new Sequence(acts);

// var progress = new n.ProgressBar(document.getElementById('experiment-progress'));

var stepList = $('steplist');

for (var i = 0; i < main.steps.length; ++i)
{
	var stepButton = document.createElement('a');
	stepButton.appendChild(document.createTextNode(i + 1));
	stepButton.href = '#' + i;
	stepButton.onclick = function() {
		document.location.reload();
	};

	if (!window.touchOnly)
		stepList.appendChild(stepButton);
}

if (!window.touchOnly && document.location.hash)
	main.playStep(parseInt(document.location.hash.substring(1)), function() {});

if (window.touchOnly)
	window.onload = bind(main, 'play');

})();
