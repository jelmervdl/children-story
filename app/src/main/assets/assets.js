var audio_assets = {
	'story': [
		'a1',
		'a11',
		'a13',
		'a15',
		'a16',
		'a2',
		'a3',
		'a4',
		'a5',
		'a6',
		'a7_1',
		'a7_2',
		'a9',
		's1_1',
		's1_2',
		's2',
		's3',
		's4_1',
		's4_2',
		's5',
		's6_1',
		's6_2',
		's7_1',
		's7_2',
		's8',
		's9',
		's10_1',
		's10_2',
		's11',
		's12',
		's13_1',
		's13_2',
		's13_3',
		's14_1',
		's14_2',
		's15',
		's16_1',
		's16_2',
		's16_3',
		's17',
		's18_1',
		's18_2',
		's19_1',
		's19_2',
		's20',
		's21_1',
		's21_2',
		's21_3',
		's22_1',
		's22_2',
		's23',
		's24_1',
		's24_2',
		's25_1',
		's25_2',
		's25_3',
		's25_4'
	],
	'memory-questions': [
		'm_auto',
		'm_bezem',
		'm_boek',
		'm_emmer',
		'm_gitaar',
		'm_hoed',
		'm_klok',
		'm_kopje',
		'm_lamp',
		'm_lepel',
		'm_paraplu',
		'm_pen',
		'm_rugzak',
		'm_schaar',
		'm_schoen',
		'm_sjaal',
		'm_skateboard',
		'm_sleutel',
		'm_stoel',
		'm_tandenborstel',
		'm_vlag',
		'm_vliegtuig',
		'm_voetbal',
		'm_zonnebril'
	],
	'test-questions': [
		'q_auto',
		'q_bezem',
		'q_boek',
		'q_emmer',
		'q_gitaar',
		'q_hoed',
		'q_klok',
		'q_kopje',
		'q_lamp',
		'q_lepel',
		'q_paraplu',
		'q_pen',
		'q_rugzak',
		'q_schaar',
		'q_schoen',
		'q_sjaal',
		'q_skateboard',
		'q_sleutel',
		'q_stoel',
		'q_tandenborstel',
		'q_vlag',
		'q_vliegtuig',
		'q_voetbal',
		'q_zonnebril'
	],
	'version-a': [
		'a10a',
		'a12a',
		'a14a',
		'a8a',
		'auto_a',
		'bezem_a',
		'boek_a',
		'emmer_a',
		'gitaar_a',
		'hoed_a',
		'klok_a',
		'kopje_a',
		'lamp_a',
		'lepel_a',
		'paraplu_a',
		'pen_a',
		'rugzak_a',
		'schaar_a',
		'schoen_a',
		'sjaal_a',
		'skateboard_a',
		'sleutel_a',
		'stoel_a',
		'tandenborstel_a',
		'vlag_a',
		'vliegtuig_a',
		'voetbal_a',
		'zonnebril_a'
	],
	'version-b': [
		'a10b',
		'a12b',
		'a14b',
		'a8b',
		'auto_b',
		'bezem_b',
		'boek_b',
		'emmer_b',
		'gitaar_b',
		'hoed_b',
		'klok_b',
		'kopje_b',
		'lamp_b',
		'lepel_b',
		'paraplu_b',
		'pen_b',
		'rugzak_b',
		'schaar_b',
		'schoen_b',
		'sjaal_b',
		'skateboard_b',
		'sleutel_b',
		'stoel_b',
		'tandenborstel_b',
		'vlag_b',
		'vliegtuig_b',
		'voetbal_b',
		'zonnebril_b'
	]
};

function find_audio_asset(name)
{
    for (var folder in audio_assets) {
		if (audio_assets[folder].indexOf(name) != -1)
			return 'assets/audio/' + folder + '/' + name + '.wav';
	}

	throw "Asset not found: " + name;
}
