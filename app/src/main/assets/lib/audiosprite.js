n.AudioSprite = function(url)
{
	var self = this;

	this.src = url;

	window.onStartPlaying = function() {
		// console.log("Start playing");
	    document.body.classList.add('playing-audio');
	};

	window.onFinishPlaying = function() {
		// console.log("Finish playing");
		document.body.classList.remove('playing-audio');
		if (self.callback) self.callback();
	};

	window.onErrorPlaying = function() {
		console.log("Oh lord");
	};
};

n.AudioSprite.prototype.load = function(next)
{
    if (next) setTimeout(next, 10);
};

n.AudioSprite.prototype.play = function(chapter, next)
{
	if (this.callback)
		this.stop();

    this.callback = next;
	globalAudio.play(
	    find_audio_asset(chapter),
	    "window.onStartPlaying()",
	    "window.onFinishPlaying()",
	    "window.onErrorPlaying()");
};

n.AudioSprite.prototype.stop = function()
{
	this.callback = null;
    globalAudio.stop();
	document.body.classList.remove('playing-audio');
};
