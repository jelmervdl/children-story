n.ProgressBar = function(element)
{
	this.element = element;
	this.value = 0;
	this.max = 0;
};

n.ProgressBar.prototype.add = function(length)
{
	this.max += length || 1;
};

n.ProgressBar.prototype.increment = function()
{
	this.value++;
	this.update();
};

n.ProgressBar.prototype.update = function()
{
    try {
		this.element.style.width = (this.value / this.max) * 100 + '%';
	} catch(e) {}
};
