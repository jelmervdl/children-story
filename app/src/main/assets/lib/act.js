n.Act = function(data)
{
	var self = this;

	this.id = data.id || undefined;

	this.stage = data.stage;

	if (!this.stage)
		throw Error("Stage not set");

	this.resources = data.resources || {};

	this.setUp = data.setUp || function() {};

	this.tearDown = data.tearDown || function() {};

	// Bind stage to each of the callbacks
	this.scenes = data.scenes;

	this.sequence = new Sequence(data.scenes.map(function(scene) {
		return function(next) {
			self.resetStage();
			scene.call(self, self.stage, next);
		};
	}));

	this.bookmarks = {};

	this.sceneDestructors = [];
};

n.Act.prototype.get = function(id)
{
	return this.resources[id];
};

n.Act.prototype.bindResources = function()
{
	for (var id in this.resources)
	{
		var el = this.stage.querySelector('*[data-resource-id="' + id + '"]');
		if (el) this.resources[id].bindToElement(el);
	}
};

n.Act.prototype.load = function(next)
{
	var loading = 1;
	
	var progress = function() {
		if (--loading == 0)
			next();
	};

	for (var id in this.resources) {
		++loading;
		this.resources[id].load(progress);
	}

	// In case there are no things to load, this should be called immediately :)
	progress();
};

n.Act.prototype.resetStage = function()
{
	this.sceneDestructors.forEach(function(destructor) {
		destructor();
	});

	this.sceneDestructors = [];
};

n.Act.prototype.setTimeout = function(callback, delay)
{
	var timeout = setTimeout(callback, delay);

	this.sceneDestructors.push(function() {
		clearTimeout(timeout);
	});
};

n.Act.prototype.clickOnce = function(elements, callback)
{
	var destructor = clickOnce(elements, callback);
	this.sceneDestructors.push(destructor);
};

n.Act.prototype.play = function(next)
{
	var act = this;

	this.bindResources(this);
			
	this.stage.classList.add('current');

	this.setUp.call(this, this.stage);

	this.callback = function() {
		act.stage.classList.remove('current');
		
		act.resetStage();
		
		act.tearDown.call(act, act.stage);
		
		next();
	};

	this.sequence.play(this.callback);
};

n.Act.prototype.mark = function(name)
{
	this.bookmarks[name] = this.sequence.step;
};

n.Act.prototype.jump = function(name)
{
	this.resetStage();

	this.sequence.playStep(this.bookmarks[name], this.callback);
};
