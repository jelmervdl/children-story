(function() {

StyleFix.register(function(css) {
	var w = innerWidth, h = innerHeight, m = Math.min(w,h);

	return css.replace(RegExp('\\bbackground\\-position:\\s*(.+?);', 'gi'), function($0, definition) {
		var precalculated = definition.replace(
			RegExp('\\b(left|right|bottom|top)\\s+(-?[\\d\\.]+)(vw|vh|vm|px|%)?', 'gi'),
			function($1, position, offset, unit) {
				var value;

				switch (position) {
					case 'left':
					case 'top':
						value = 0;
						break;

					case 'bottom':
						value = h;
						break;

					case 'right':
						value = w;
						break;
				}

				switch (unit) {
					case 'vw':
						offset = offset * w / 100;
						break;

					case 'vh':
						offset = offset * h / 100;
						break;

					case 'vm':
						offset = num * m / 100;
						break;

					case '%':
						if (position == 'left' || position == 'top')
							return offset + '%';
						else
							throw Error('Cannot rewrite ' + $0 + ' due to ' + $1);

					case 'px':
					default:
						offset = num;
						break;
				}

				console.log($1 + " -> " + (value + offset + 'px'));

				return value + offset + 'px';
			});

		return 'background-position: ' + precalculated;
	});
});

})();
