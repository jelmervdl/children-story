n.Measurements = {
	subjectId: s4(),
	measurements: []
};

n.Measurements.push = function(measurement)
{
	this.measurements.push(measurement);
};

n.Measurements.submit = function(personalData)
{
	window.activity.submitMeasurements(JSON.stringify([{
		subject_id: this.subjectId,
		personal_data: personalData,
		measurements: this.measurements 
	}]));

	// Reset
	this.subjectId = s4();
	this.measurements = [];
}

n.Measurement = function(id)
{
	this.id = id;
};

n.Measurement.prototype.start = function()
{
	this.startTime = new Date().getTime();
};

n.Measurement.prototype.stop = function(choice)
{
	if (this.stopTime)
		return;

	this.choice = choice;
	this.stopTime = new Date().getTime();
	n.Measurements.push(this);
};
