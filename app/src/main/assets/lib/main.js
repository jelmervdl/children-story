Array.prototype.shuffle = function shuffle()
{
	for (var i = this.length - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var tmp = this[i];
		this[i] = this[j];
		this[j] = tmp;
	}

	return this;
};

window.$ = function $(id) {
	return document.getElementById(id);
};

window.s4 = function s4() {
	return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
};

window.random = function random(min, max) {
	return min + Math.floor(1 + Math.random() * (max - min));
};

window.clickOnce = function listenOnce(elements, callback)
{
	var listeners = Array.prototype.map.call(elements, function(element) {
		return {
			element: element,
			callback: function(e) {
				if (callback(element) !== false) {
					e.preventDefault();
					removeListeners();
				}
			}
		};
	});

	var addListeners = function() {
		listeners.forEach(function(listener) {
			listener.element.classList.add('clickable');
			if (window.touchOnly)
				listener.element.addEventListener('touchend', listener.callback, false);
			else
				listener.element.addEventListener('mousedown', listener.callback, false);
		});
	};

	var removeListeners = function() {
		listeners.forEach(function(listener) {
			listener.element.classList.remove('clickable');
			if (window.touchOnly)
				listener.element.removeEventListener('touchend', listener.callback, false);
			else
				listener.element.removeEventListener('mousedown', listener.callback, false);
		});

		listeners = []; // To make sure you can only call removeListeners effectively once.
	};

	addListeners();

	return removeListeners;
};

window.listenOnce = function listenOnce(element, event, callback)
{
	var next = function() {
		console.log(event + " fired");
		element.removeEventListener(next);
		callback();
	};

	element.addEventListener(event, callback);
};

window.after = function after(tasks, callback)
{
	var running = tasks.length;
	
	var onfinish = function() {
		if (--running === 0)
			callback();
	};

	Array.prototype.forEach.call(tasks, function(task) {
		task(onfinish);
	});
};

window.bind = function bind(object, method)
{
	return function(arg) {
		return object[method].call(object, arg);
	};
};

// Today, n is my namespace.
window.n = {};
